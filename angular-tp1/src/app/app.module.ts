import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PlayersPatternComponent } from './players-pattern/players-pattern.component';
import { RegisterPlayersComponent } from './register-players/register-players.component';

@NgModule({
  declarations: [
    AppComponent,
    PlayersPatternComponent,
    RegisterPlayersComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit } from '@angular/core';
import { Players } from './../models/players';

@Component({
  selector: 'app-register-players',
  templateUrl: './register-players.component.html',
  styleUrls: ['./register-players.component.css']
})
export class RegisterPlayersComponent implements OnInit {
  players:Players[];
  constructor() { 
    this.players=[];
  }

  ngOnInit(): void {
  }
  guardar(fullname:string,age:string,url:string):boolean{
    this.players.push(new Players(fullname,age,url));
    return false;
  }

}

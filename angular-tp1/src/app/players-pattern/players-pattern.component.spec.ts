import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayersPatternComponent } from './players-pattern.component';

describe('PlayersPatternComponent', () => {
  let component: PlayersPatternComponent;
  let fixture: ComponentFixture<PlayersPatternComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlayersPatternComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayersPatternComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

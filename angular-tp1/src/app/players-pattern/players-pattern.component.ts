import { Component, OnInit, Input,HostBinding } from '@angular/core';
import { Players } from './../models/players';

@Component({
  selector: 'app-players-pattern',
  templateUrl: './players-pattern.component.html',
  styleUrls: ['./players-pattern.component.css']
})
export class PlayersPatternComponent implements OnInit {
  @Input() player: Players;
  @HostBinding('attr.class') cssClass = 'back_color';

  constructor() { 

  }

  ngOnInit(): void {
  }
}

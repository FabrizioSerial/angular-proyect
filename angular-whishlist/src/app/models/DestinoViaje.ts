import { url } from "inspector";

export class DestinoViaje{
    nombre:string;
    imageUrl:string;

    constructor(nombre:string,imageURL:string){
        this.nombre= nombre;
        this.imageUrl=imageURL;
    }
}